"""Reisa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
# from django.conf.urls import (
#     handler403
# )
from django.conf import settings
from django.conf.urls.static import static


import debug_toolbar

from reisa.views import home


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', home, name="home"),
    url(r'^polizas/', include('polizas.urls', namespace="polizas")),
    url(r'^inventarios/', include('inventario.urls', namespace="inventarios")),
    url(r'^usuarios/', include('usuarios.urls', namespace="usuarios")),
    url(r'^tickets/', include('tickets.urls', namespace="tickets")),
    url(r'^froala_editor/', include('froala_editor.urls')),
    url(r'^__debug__/', include(debug_toolbar.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

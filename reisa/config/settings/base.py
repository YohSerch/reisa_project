from unipath import Path
import os

BASE_DIR = Path(__file__).ancestor(3)

SECRET_KEY = 'xaxy84%$x@ze3w1k1$w)h3u#c+os*c7qfs3#_lg#e%&a+3hza8'

ALLOWED_HOSTS = [
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'polizas',
    'inventario',
    'usuarios',
    'tickets',
    'froala_editor',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR.child("templates"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'inventario.context_processors.poliza_actual',
                'inventario.context_processors.equipo_actual'
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

#Static files dirs
STATICFILES_DIRS = [
    BASE_DIR.child("static"),
]

MEDIA_ROOT = BASE_DIR.child("media")

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR.child('staticfiles')
MEDIA_URL = '/media/'

LOGIN_URL = 'usuarios:login'
LOGIN_REDIRECT_URL = 'home'
LOGOUT_REDIRECT_URL = 'usuarios:login'

AUTH_USER_MODEL = 'usuarios.User'

FROALA_EDITOR_THEME = 'dark'

FROALA_EDITOR_OPTIONS = {
    'toolbarButtons': ['undo', 'redo', '|', 'fontSize', 'bold', 'italic',
                       'underline', 'strikeThrough', 'outdent', 'indent',
                       'align', 'clearFormatting', 'insertTable', 'html',
                       'formatUL', 'insertLink', 'fullscreen'],
    'height': '250px'
}

FROALA_EDITOR_PLUGINS = ('align', 'char_counter', 'code_beautifier',
                         'code_view', 'colors', 'font_family', 'font_size',
                         'fullscreen', 'line_breaker','link', 'lists',
                         'paragraph_format', 'table', 'url')

FROALA_INCLUDE_JQUERY = False

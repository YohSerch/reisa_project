from polizas.models import Poliza
from .models import Equipo


def poliza_actual(request):
    try:
        folio = request.resolver_match.kwargs['poliza']
    except KeyError:
        folio = None
    if folio:
        poliza = Poliza.objects.get(folio=folio)
        return {
            'poliza': poliza
        }
    return {}


def equipo_actual(request):
    try:
        id_equipo = request.resolver_match.kwargs['equipo']
    except KeyError:
        id_equipo = None
    if id_equipo:
        equipo = Equipo.objects.get(id=id_equipo)
        return {
            'equipo': equipo
        }
    return {}

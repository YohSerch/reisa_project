from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$',
		inventario_poliza,
		name="list"),
	url(r'^(?P<poliza>[\w]+)/equipos/(?P<pk>[\w\s\d-]+)/$',
		EquipoDetailView.as_view(),
		name="detail"),
	url(r"^(?P<poliza>[\w]+)/equipos/$",
		EquipoListView.as_view(),
		name="equipos"
		),
	url(r"^(?P<poliza>[\w]+)/equipos/export$",
		export_xlsx,
		name="equipos_export"
		),
	url(r'^(?P<poliza>[\w]+)/equipos/create$',
		EquipoCreateView.as_view(),
		name="create"),
	url(r'^(?P<poliza>[\w]+)/equipos/create/multiple$',
		carga_equipos,
		name="create_massive"),
	url(r'^(?P<poliza>[\w]+)/equipos/update/(?P<pk>[\w\s\d-]+)/$',
		EquipoUpdateView.as_view(),
		name="update"),
	url(r'^(?P<poliza>[\w]+)/equipos/inhabilitar/(?P<pk>[\w\s\d-]+)/$',
		inhabilitar_equipo,
		name="inhabilitar"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/licencias/$',
		LicenciaListView.as_view(),
		name="licencias_list"
		),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/licencias/create/$',
		LicenciaCreateView.as_view(),
		name="licencia_create"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/licencias/update/(?P<pk>[\d]+)/$',
		LicenciaUpdateView.as_view(),
		name="licencia_update"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/licencias/delete/(?P<pk>[\d]+)/$',
		LicenciaDeleteView.as_view(),
		name="licencia_delete"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/historial/$',
		TicketListView.as_view(),
		name="tickets_list"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/software/$',
		SoftwareListView.as_view(),
		name="software_list"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/software/create/$',
		SoftwareCreateView.as_view(),
		name="software_create"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/software/update/(?P<pk>[\d]+)/$',
		SoftwareUpdateView.as_view(),
		name="software_update"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/software/delete/(?P<pk>[\d]+)/$',
		SoftwareDeleteView.as_view(),
		name="software_delete"),
	url(r'^(?P<poliza>\w+)/equipos/(?P<equipo>[\w\s\d-]+)/QRCode',
		show_qrcode,
		name="show_qrcode"),
	# URLS AJAX
	url(r'^equipos/$',
		equipos_por_poliza,
		name="equipos_list_ajax"),
	url(r'^(?P<poliza>\w+)/impresoras/create/$',
		ImpresoraCreateView.as_view(),
		name="impresoras_create"),
	url(r'^(?P<poliza>\w+)/impresoras/$',
		ImpresoraListView.as_view(),
		name="impresoras"),
	url(r'^(?P<poliza>\w+)/impresoras/(?P<pk>[\d]+)/update/$',
		ImpresoraUpdateView.as_view(),
		name="impresoras_update"),
	url(r'^(?P<poliza>\w+)/impresoras/(?P<pk>[\d]+)/delete/$',
		ImpresoraDeleteView.as_view(),
		name="impresoras_delete"),
	url(r'^(?P<poliza>\w+)/servidores/$',
		ServidorListView.as_view(),
		name="servidores"),
	url(r'^(?P<poliza>\w+)/servidores/create$',
		ServidorCreateView.as_view(),
		name="servidores_create"),
	url(r'^(?P<poliza>\w+)/servidores/(?P<pk>[\d]+)/$',
		ServidorDetailView.as_view(),
		name="servidores_detail"),
	url(r'^(?P<poliza>\w+)/servidores/(?P<pk>[\d]+)/update/$',
		ServidorUpdateView.as_view(),
		name="servidores_update"),
	url(r'^(?P<poliza>\w+)/servidores/(?P<pk>[\d]+)/delete/$',
		ServidorDeleteView.as_view(),
		name="servidores_delete"),
]
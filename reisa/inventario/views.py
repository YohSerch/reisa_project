from datetime import datetime
import sys

from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_safe
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.db.models import Count, Q, Subquery, OuterRef, IntegerField
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin

from reisa.utils import load_json, filter_list, date_generator, WriteToExcel
from tickets.models import TicketConPoliza
from .models import Equipo, Licencia, Software, Impresora, Servidor
from .forms import *
from polizas.models import Poliza


# Create your views here.
@login_required
@require_safe
def equipos_por_poliza(request):
    print(request.GET.get('poliza'))
    poliza = get_object_or_404(Poliza, folio = request.GET.get('poliza'))
    data = {
        'poliza_status': poliza.status,
        'equipos': list(Equipo.objects.filter(id_poliza = poliza).values_list('id'))
    }
    return JsonResponse(data)


@login_required
@permission_required('polizas.list_equips', raise_exception=True)
@require_safe
def inventario_poliza(request, template_name = "inventario/inventario_list.html"):
    poliza_list = Poliza.objects.annotate(
        equip_count=Subquery(
            Equipo.objects.filter(
                status = True, 
                id_poliza=OuterRef('folio')
            ).values('id_poliza')
            .annotate(cnt=Count('id'))
            .values('cnt'),
            output_field=IntegerField()
        )
    ).order_by('folio')
    page = request.GET.get('page')
    paginator = Paginator(poliza_list, 8)
    try:
        polizas = paginator.page(page)
    except PageNotAnInteger:
        polizas = paginator.page(1)
    except EmptyPage:
        polizas = paginator.page(paginator.num_pages)
    return render(request, template_name, {'polizas': polizas})


# Vistas para el manejo de equipos dentro del sistema
class EquipoDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = "inventario/equipo_detail.html"
    context_object_name = "equipo"
    permission_required = 'inventario.detail_equipo'
    raise_exception = True
    model = Equipo

    def get_queryset(self):
        return Equipo.objects.select_related('usuario').filter(
            id=self.kwargs['pk'], id_poliza=self.kwargs['poliza'])


class EquipoListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = "inventario/equipos_list.html"
    context_object_name = "equipos"
    permission_required = 'inventario.list_equipos'
    raise_exception = True
    paginate_by = 8

    def get_queryset(self):
        if self.request.user.is_designated_person:
            folio = self.request.user.poliza.folio
            if folio != self.kwargs['poliza']:
                raise PermissionDenied
        self.poliza = get_object_or_404(Poliza, folio=self.kwargs['poliza'])
        return Equipo.objects.filter(id_poliza=self.poliza).order_by('id')


class EquipoCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    form_class = EquipoCreateForm
    template_name = "inventario/equipo_create_form.html"
    model = Equipo
    permission_required = 'inventario.add_equipo'
    success_message = "El equipo %(id)s ha sido creado con éxito"
    raise_exception = True

    def form_valid(self, form):
        poliza = Poliza.objects.get(folio = self.kwargs['poliza'])
        form.instance.id_poliza = poliza
        form.instance.usuario = self.request.user
        return super(EquipoCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('inventarios:equipos', kwargs={'poliza': self.kwargs['poliza']})


class EquipoUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    form_class = EquipoUpdateForm
    template_name = "inventario/equipo_update_form.html"
    model = Equipo
    permission_required = 'inventario.change_equipo'
    raise_exception = True
    success_message = "El equipo ha sido actualizado con éxito"

    def form_valid(self, form):
        form.instance.usuario = self.request.user
        if form.instance.status and form.instance.removed_at != None:
            form.instance.removed_at = None
        return super(EquipoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('inventarios:equipos', kwargs={'poliza': self.kwargs['poliza']})

@login_required
@permission_required('inventario.delete_equipo', raise_exception=True)
@require_safe
def inhabilitar_equipo(request, poliza, pk):
    equipo = get_object_or_404(Equipo, id=pk)
    message = ""
    equipo.status = not equipo.status
    if equipo.status is False:
        equipo.removed_at = datetime.now()
        message = "inhabilitado"
    else:
        equipo.removed_at = None
        message = "habilitado"
    equipo.save()
    messages.success(request, 'Equipo {0} con éxito'.format(message))
    return redirect('inventarios:equipos', poliza)


# Vistas para el manejo de licencias por equipo
class LicenciaListView(LoginRequiredMixin, ListView):
    model = Licencia
    template_name = "inventario/licencias_list.html"
    context_object_name = "licences"
    paginate_by = 8
    poliza = None
    equipo = None

    def get_queryset(self):
        self.equipo = get_object_or_404(Equipo, id=self.kwargs['equipo'])
        return Licencia.objects.filter(id_equipo=self.equipo).order_by('id')


class LicenciaCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Licencia
    template_name = "inventario/licencia_create_form.html"
    form_class = LicenciaCreateForm
    permission_required = 'inventario.add_licencia'
    raise_exception = True
    success_message = "La licencia para el software %(software)s ha sido creada con éxito."

    def form_valid(self, form):
        equipo = Equipo.objects.get(id = self.kwargs['equipo'])
        form.instance.id_equipo = equipo
        return super(LicenciaCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('inventarios:licencias_list', kwargs={'poliza': self.kwargs['poliza'], 'equipo': self.kwargs['equipo']})


class LicenciaUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Licencia
    template_name = "inventario/licencia_update_form.html"
    form_class = LicenciaCreateForm
    permission_required = 'inventario.change_licencia'
    raise_exception = True
    success_message = "La licencia para el software %(software)s fue modificada con éxito."

    def get_success_url(self):
        return reverse_lazy('inventarios:licencias_list', kwargs={'poliza': self.kwargs['poliza'], 'equipo': self.kwargs['equipo']})


class LicenciaDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Licencia
    template_name = "inventario/licencia_delete_confirm.html"
    permission_required = 'inventario.delete_licencia'

    def get_success_url(self):
        return reverse_lazy('inventarios:licencias_list', kwargs={'poliza': self.kwargs['poliza'], 'equipo': self.kwargs['equipo']})


class SoftwareListView(LoginRequiredMixin, ListView):
    model = Software
    template_name = "inventario/software_list.html"
    context_object_name = "software"
    poliza = None
    equipo = None
    paginate_by = 8
    
    def get_queryset(self):
        self.equipo = get_object_or_404(Equipo, id = self.kwargs['equipo'])
        return Software.objects.filter(equipo = self.equipo)


class SoftwareCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Software
    form_class = SoftwareCreateForm
    template_name = "inventario/software_create_form.html"
    permission_required = "inventario.add_software"
    raise_exception = True
    success_message = "El software %(nombre)s ha sido creado con éxito"

    def form_valid(self, form):
        equipo = get_object_or_404(Equipo, id = self.kwargs['equipo'])
        form.instance.equipo = equipo
        return super(SoftwareCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('inventarios:software_list', kwargs={'poliza': self.kwargs['poliza'], 'equipo': self.kwargs['equipo']})


class SoftwareUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Software
    form_class = SoftwareCreateForm
    template_name = "inventario/software_update_form.html"
    permission_required = "inventario.change_software"
    raise_exception = True
    success_message = "El software %(nombre)s ha sido modificado con éxito"

    def get_success_url(self):
        return reverse_lazy('inventarios:software_list', kwargs={'poliza': self.kwargs['poliza'], 'equipo': self.kwargs['equipo']})


class SoftwareDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Software
    template_name = "inventario/software_delete_confirm.html"
    permission_required = "inventario.delete_licencia"

    def get_success_url(self):
        return reverse_lazy('inventarios:software_list', kwargs={'poliza': self.kwargs['poliza'], 'equipo': self.kwargs['equipo']})


class TicketListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = TicketConPoliza
    template_name = "inventario/tickets_por_equipo.html"
    context_object_name = "tickets"
    permission_required = "tickets.list_by_equipo"
    raise_exception = True

    def get_queryset(self):
        if self.request.user.is_designated_person:
            folio = self.request.user.poliza.folio
            if folio != self.kwargs['poliza']:
                raise PermissionDenied
        self.poliza = get_object_or_404(Poliza, folio=self.kwargs['poliza'])
        self.equipo = get_object_or_404(Equipo, id=self.kwargs['equipo'])
        tickets = TicketConPoliza.objects.select_related('atendido_por').filter(Q(poliza=self.poliza), Q(equipo=self.equipo), Q(status='5') | Q(status='4') | Q(status='3'))
        return filter_list(tickets, self.request.GET).order_by('created_at')

    def get_context_data(self, **kwargs):
        context = super(TicketListView, self).get_context_data(**kwargs)
        context['dates'] = date_generator()
        return context


# NUEVA FUNCION JSON
@login_required
@permission_required('inventario.add_equipo', raise_exception=True)
def carga_equipos(request, poliza):
    poliza = get_object_or_404(Poliza, folio=poliza)
    if request.method == "POST":
        records = load_json(request, 'inventarios:create_massive')
        if records == -1:
            messages.error(request, "Formato de archivo invalido.")
            return redirect("inventarios:create_massive", poliza.folio)

        for equipo in records:
            e = Equipo.objects.create(id=equipo['id'], marca=equipo['marca'], service_tag=equipo['service_tag'], sistema_operativo=equipo['sistema_operativo'], disco_duro=equipo['disco_duro'], ram=equipo['ram'], graficos=equipo['graficos'], procesador=equipo['procesador'], username=equipo['username'], id_poliza=poliza, usuario=request.user)
            if equipo['software']:
                try:
                    Software.objects.bulk_create([ Software(nombre=nombre, equipo=e)  for nombre in equipo['software']])
                except:
                    messages.error(request, 'Ocurrio un error al almacenar el software del equipo {0}, revisa tu archivo e intenta de nuevo. {1}'.format(e.id, sys.exc_info()[1]))
                    return redirect('inventarios:create_massive')
            if equipo['licencias']:
                try:
                    Licencia.objects.bulk_create([ Licencia(software=licencia['software'], codigo=licencia['codigo'], fecha_inicio=licencia['fecha_inicio'], fecha_final=licencia['fecha_final'], numero_licencia=licencia['numero_licencia'], id_equipo=e) for licencia in equipo['licencias'] ])
                except:
                    messages.error(request, 'Ocurrio un error al almacenar las licencias del equipo {0}, revisa tu archivo e intenta de nuevo. {1}'.format(e.id, sys.exc_info()[1]))
                    return redirect('inventarios:create_massive')
        messages.success(request, "Equipos creados con éxito")
        return redirect('inventarios:equipos', poliza.folio)
    return render(request, "inventario/carga_masiva_equipo.html")


@login_required
@permission_required('inventario.export_equipos', raise_exception=True)
@require_safe
def export_xlsx(request, poliza):
    poliza = get_object_or_404(Poliza, folio=poliza)
    equipos = Equipo.objects.filter(id_poliza=poliza)
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=inventario_{0}.xlsx'.format(poliza.nombre)
    xlsx_data = WriteToExcel(poliza, equipos)
    response.write(xlsx_data)
    return response


@login_required
@require_safe
def show_qrcode(request, poliza, equipo):
    if request.user.is_designated_person:
        folio = request.user.poliza.folio
        if folio != request.GET.get('poliza'):
            raise PermissionDenied
    poliza = get_object_or_404(Poliza, folio=poliza)
    equipo = get_object_or_404(Equipo, id_poliza=poliza, id=equipo)
    if not equipo.qrcode:
        equipo.generate_qrcode()
    return render(request, 'inventario/show_qrcode.html')


class ImpresoraListView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, ListView):
    model = Impresora
    template_name = "inventario/impresora_list.html"
    context_object_name = "impresoras"
    permission_required = "inventario.list_impresora"
    raise_exception = True

    def get_queryset(self):
        if self.request.user.is_designated_person:
            folio = self.request.user.poliza.folio
            if folio != self.kwargs['poliza']:
                raise PermissionDenied
        return Impresora.objects.filter(poliza=self.kwargs['poliza']).order_by('id')


class ImpresoraCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Impresora
    form_class = ImpresoraCreateForm
    template_name = "inventario/impresora_create_form.html"
    permission_required = 'inventario.add_impresora'
    success_message = "La impresora %(modelo)s ha sido creado con éxito"
    raise_exception = True

    def form_valid(self, form):
        poliza = Poliza.objects.get(folio=self.kwargs['poliza'])
        form.instance.poliza = poliza
        return super(ImpresoraCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('inventarios:impresoras', kwargs={'poliza': self.kwargs['poliza']})


class ImpresoraUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Impresora
    form_class = ImpresoraCreateForm
    template_name = "inventario/impresora_update_form.html"
    permission_required = "inventario.change_impresora"
    success_message = "La impresora ha sido actualizada con éxito"
    raise_exception = True

    def get_success_url(self):
        return reverse_lazy('inventarios:impresoras', kwargs={'poliza': self.kwargs['poliza']})


class ImpresoraDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Impresora
    template_name = "inventario/impresora_delete_confirm.html"
    permission_required = "inventario.delete_impresora"
    success_message = "La impresora fue eliminada con éxito"
    raise_exception = True

    def get_success_url(self):
        return reverse_lazy('inventarios:impresoras', kwargs={'poliza': self.kwargs['poliza']})


class ServidorListView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, ListView):
    model = Servidor
    template_name = "inventario/servidor_list.html"
    context_object_name = "servidores"
    permission_required = "inventario.list_servidor"
    raise_exception = True

    def get_queryset(self):
        if self.request.user.is_designated_person:
            folio = self.request.user.poliza.folio
            if folio != self.kwargs['poliza']:
                raise PermissionDenied
        return Servidor.objects.filter(poliza=self.kwargs['poliza']).order_by('nombre')


class ServidorCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Servidor
    form_class = ServidorCreateForm
    template_name = "inventario/servidor_create_form.html"
    permission_required = "inventario.add_servidor"
    raise_exception = True
    success_message = "El servidor %(nombre)s ha sido creado con éxito"

    def form_valid(self, form):
        poliza = Poliza.objects.get(folio=self.kwargs['poliza'])
        form.instance.poliza = poliza
        return super(ServidorCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('inventarios:servidores', kwargs={'poliza': self.kwargs['poliza']})


class ServidorUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Servidor
    form_class = ServidorCreateForm
    template_name = "inventario/servidor_update_form.html"
    permission_required = "inventario.change_servidor"
    raise_exception = True
    success_message = "El servidor fue actualizado con éxito"

    def get_success_url(self):
        return reverse_lazy('inventarios:servidores', kwargs={'poliza': self.kwargs['poliza']})


class ServidorDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Servidor
    template_name = "inventario/servidor_delete_confirm.html"
    permission_required = "inventario.delete_servidor"
    raise_exception = True
    success_message = "El servidor fue eliminado con éxito"

    def get_success_url(self):
        return reverse_lazy('inventarios:servidores', kwargs={'poliza': self.kwargs['poliza']})


class ServidorDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Servidor
    context_object_name = "servidor"
    template_name = "inventario/servidor_detail.html"
    permission_required = 'inventario.detail_servidor'
    raise_exception = True

# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-15 15:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0009_auto_20170810_1800'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipo',
            name='removed_at',
            field=models.DateField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='equipo',
            name='status',
            field=models.BooleanField(default=True),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-12 21:51
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0002_auto_20170712_2107'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipo',
            name='created_at',
            field=models.DateField(default=datetime.datetime(2017, 7, 12, 21, 51, 48, 693611)),
        ),
    ]

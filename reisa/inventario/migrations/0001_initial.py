# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-10 21:38
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('polizas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('marca', models.CharField(max_length=30)),
                ('service_tag', models.CharField(max_length=50)),
                ('sistema_operativo', models.CharField(max_length=50)),
                ('disco_duro', models.CharField(max_length=20)),
                ('ram', models.IntegerField()),
                ('graficos', models.CharField(max_length=50)),
                ('procesador', models.CharField(max_length=50)),
                ('id_poliza', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polizas.Poliza')),
            ],
        ),
        migrations.CreateModel(
            name='Licencia',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('software', models.CharField(max_length=100)),
                ('codigo', models.CharField(max_length=100)),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField()),
                ('numero_licencia', models.IntegerField()),
                ('id_equipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventario.Equipo')),
            ],
        ),
    ]

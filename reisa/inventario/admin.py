from django.contrib import admin
from .models import Equipo, Licencia, Software, Impresora, Servidor

class EquipoAdmin(admin.ModelAdmin):
	pass


class ImpresoraAdmin(admin.ModelAdmin):
	pass


class ServidorAdmin(admin.ModelAdmin):
	pass


class LicenciaAdmin(admin.ModelAdmin):
	pass


class SoftwareAdmin(admin.ModelAdmin):
	pass


admin.site.register(Equipo, EquipoAdmin)
admin.site.register(Licencia, LicenciaAdmin)
admin.site.register(Software, SoftwareAdmin)
admin.site.register(Impresora, ImpresoraAdmin)
admin.site.register(Servidor, ServidorAdmin)

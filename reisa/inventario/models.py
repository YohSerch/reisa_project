from django.utils import timezone
from django.urls import reverse
from django.core.files.uploadedfile import InMemoryUploadedFile
from qrcode import QRCode
from qrcode.constants import ERROR_CORRECT_L
from io import BytesIO
from django.db import models

from polizas.models import Poliza
from usuarios.models import User


class Equipo(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    marca = models.CharField(max_length=30, null=False)
    service_tag = models.CharField(max_length=50, null=False)
    sistema_operativo = models.CharField(max_length=50, null=False)
    disco_duro = models.CharField(max_length=20)
    ram = models.IntegerField(null=False)
    graficos = models.CharField(max_length=50, null=False)
    procesador = models.CharField(max_length=50, null=False)
    id_poliza = models.ForeignKey(Poliza, on_delete=models.CASCADE, null=False)
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    status = models.BooleanField(null=False, default=True)
    qrcode = models.ImageField(upload_to='qrcode', blank=True, null=True)
    username = models.CharField(max_length=60, blank=True, null=True)
    created_at = models.DateField(default=timezone.now)
    updated_at = models.DateField(auto_now=True)
    removed_at = models.DateField(default=None, null=True)

    class Meta:
        permissions = (
            ("detail_equipo", "Can see the equipo detail"),
            ("list_equipos", "Can list equipos"),
            ("export_equipos", "Can export xlsx file"))

    def get_absolute_url(self):
        host = "http://reisavirtual.ddns.net:81"
        path = reverse('inventarios:detail', args=[str(self.id_poliza.folio), str(self.id)])
        url = "{0}{1}".format(host, path)
        return url

    def generate_qrcode(self):
        qr = QRCode(
            version=1,
            error_correction=ERROR_CORRECT_L,
            box_size=14,
            border=2
        )
        print(self.get_absolute_url())
        qr.add_data(self.get_absolute_url())
        qr.make(fit=True)

        img = qr.make_image()

        buffer = BytesIO()
        img.save(buffer)

        filename = "computer-{0}.png".format(self.id)
        filebuffer = InMemoryUploadedFile(
            buffer, None, filename, 'image/png', buffer.tell(), None)
        self.qrcode.save(filename, filebuffer)

    def save(self, *args, **kwargs):
        super(Equipo, self).save(*args, **kwargs)

    def __str__(self):
        return self.id


class Licencia(models.Model):

    id = models.AutoField(primary_key=True)
    software = models.CharField(max_length=100, null=False)
    codigo = models.CharField(max_length=100, null=False)
    fecha_inicio = models.DateField(null=False)
    fecha_final = models.DateField(null=False)
    numero_licencia = models.IntegerField()
    id_equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)

    def __str__(self):
        return "{0} - {1}".format(self.id_equipo, self.software)


class Software(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)


class Impresora(models.Model):
    id = models.AutoField(primary_key=True)
    marca = models.CharField(max_length=50, blank=True, null=True)
    modelo = models.CharField(max_length=100)
    ip = models.GenericIPAddressField()
    poliza = models.ForeignKey(Poliza, on_delete=models.CASCADE)

    def __str__(self):
        return self.modelo

    class Meta:
        permissions = (
            ("list_impresora", "Can list impresoras"),
        )


class Servidor(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(primary_key=False, max_length=100)
    modelo = models.CharField(max_length=100)
    sistema_operativo = models.CharField(max_length=100, null=False)
    procesador = models.CharField(max_length=100, null=False)
    disco_duro = models.CharField(max_length=20)
    ram = models.IntegerField(null=False)
    poliza = models.ForeignKey(Poliza, on_delete=models.CASCADE)
    usuario = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=150, blank=True, null=True)
    ip = models.GenericIPAddressField()
    puerto = models.CharField(max_length=30, blank=True, null=True)
    created_at = models.DateField(default=timezone.now)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        permissions = (
            ("detail_servidor", "Can see the servidor detail"),
            ("list_servidor", "Can list servidores"),
        )

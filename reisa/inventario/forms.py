from django.forms import ModelForm, TextInput, NumberInput, DateInput
from django.utils.translation import ugettext_lazy as _


from .models import Equipo, Licencia, Software, Impresora, Servidor


class EquipoCreateForm(ModelForm):
	class Meta:
		model = Equipo
		exclude = ['id_poliza', 'usuario', 'removed_at', 'status', 'qrcode']
		widgets = {
			'id': TextInput(attrs= {'placeholder': 'Id del equipo', 'class': 'form-control'}),
			'marca': TextInput(attrs={'placeholder': 'Marca', 'class': 'form-control'}),
			'service_tag': TextInput(attrs={'placeholder': 'Service Tag', 'class': 'form-control'}),
			'sistema_operativo': TextInput(attrs={'placeholder': 'Sistema Operativo', 'class': 'form-control'}),
			'disco_duro': TextInput(attrs={'placeholder': 'Disco Duro', 'class': 'form-control'}),
			'ram': NumberInput(attrs={'placeholder': 'RAM', 'class': 'form-control'}),
			'graficos': TextInput(attrs={'placeholder': 'Gráficos', 'class': 'form-control'}),
			'procesador': TextInput(attrs={'placeholder': 'Procesador', 'class': 'form-control'}),
			'username': TextInput(attrs={'placeholder': 'Nombre de usuario', 'class': 'form-control'}),
			'created_at': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker', 'class': 'form-control'})
		}
		labels = {
			'created_at': _('Fecha de registro'),
		}

class EquipoUpdateForm(ModelForm):
	class Meta:
		model = Equipo
		exclude = ['id', 'id_poliza', 'usuario', 'removed_at', 'status', 'qrcode']
		widgets = {
			'marca': TextInput(attrs={'placeholder': 'Marca', 'class': 'form-control'}),
			'service_tag': TextInput(attrs={'placeholder': 'Service Tag', 'class': 'form-control'}),
			'sistema_operativo': TextInput(attrs={'placeholder': 'Sistema Operativo', 'class': 'form-control'}),
			'disco_duro': TextInput(attrs={'placeholder': 'Disco Duro', 'class': 'form-control'}),
			'ram': NumberInput(attrs={'placeholder': 'RAM', 'class': 'form-control'}),
			'graficos': TextInput(attrs={'placeholder': 'Gráficos', 'class': 'form-control'}),
			'procesador': TextInput(attrs={'placeholder': 'Procesador', 'class': 'form-control'}),
			'username': TextInput(attrs={'placeholder': 'Nombre de usuario', 'class': 'form-control'}),
			'created_at': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker', 'class': 'form-control'})
		}
		labels = {
			'created_at': _('Fecha de registro'),
		}


class LicenciaCreateForm(ModelForm):
	class Meta:
		model = Licencia
		exclude = ['id_equipo', 'usuario']
		widgets = {
			'software': TextInput(attrs= {'placeholder': 'Software', 'class': 'form-control'}),
			'codigo': TextInput(attrs={'placeholder': 'Código', 'class': 'form-control'}),
			'fecha_inicio': DateInput(attrs={'placeholder': 'Fecha de Inicio', 'class': 'datepicker form-control'}),
			'fecha_final': DateInput(attrs={'placeholder': 'Fecha de Fin', 'class': 'datepicker form-control'}),
			'numero_licencia': TextInput(attrs={'placeholder': 'Número de licencia', 'class': 'form-control'}),
		}
		labels = {
			'numero_licencia': _('Número de licencia'),
			'fecha_inicio': _('Fecha de inicio'),
			'fecha_final': _('Fecha de termino'),
		}

class SoftwareCreateForm(ModelForm):
	class Meta:
		model = Software
		fields = ['nombre']
		widgets = {
			'nombre': TextInput(attrs={'placeholder': 'Nombre del software', 'class': 'form-control'}),
		}
		labels = {
			'nombre': _('Nombre del software')
		}


class ImpresoraCreateForm(ModelForm):
	class Meta:
		model = Impresora
		exclude = ['id', 'poliza']
		widgets = {
			'marca': TextInput(attrs={'placeholder': 'Marca', 'class': 'form-control'}),
			'modelo': TextInput(attrs={'placeholder': 'Modelo', 'class': 'form-control'}),
			'ip': TextInput(attrs={'placeholder': 'IP', 'class': 'form-control'})
		}


class ServidorCreateForm(ModelForm):
	class Meta:
		model = Servidor
		exclude = ['id', 'poliza', 'created_at', 'updated_at']
		widgets = {
			'nombre': TextInput(attrs={'placeholder': 'Nombre del servidor', 'class': 'form-control'}),
			'modelo': TextInput(attrs={'placeholder': 'Modelo', 'class': 'form-control'}),
			'sistema_operativo': TextInput(attrs={'placeholder': 'Sistema Operativo', 'class': 'form-control'}),
			'disco_duro': TextInput(attrs={'placeholder': 'Disco Duro', 'class': 'form-control'}),
			'ram': NumberInput(attrs={'placeholder': 'RAM', 'class': 'form-control'}),
			'procesador': TextInput(attrs={'placeholder': 'Procesador', 'class': 'form-control'}),
			'usuario': TextInput(attrs={'placeholder': 'Usuario', 'class': 'form-control'}),
			'password': TextInput(attrs={'placeholder': 'Contraseña', 'class': 'form-control'}),
			'ip': TextInput(attrs={'placeholder': 'IP', 'class': 'form-control'}),
			'puerto': TextInput(attrs={'placeholder': 'Puerto', 'class': 'form-control'}),
			
		}
		labels = {
			'password': _('Contraseña')
		}

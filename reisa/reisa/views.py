from datetime import datetime

from django.shortcuts import render, redirect
from django.db.models import Count, Subquery, OuterRef, IntegerField
from django.db.models.functions import TruncMonth
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_safe

from tickets.models import TicketConPoliza
from polizas.models import Poliza

@login_required
@require_safe
def home(request):
	now = datetime.now()
	months = 12 - now.month
	title = "Estadisticas Generales"
	poliza = None
	if months > 0:
		initial_date = now.replace(day = 1, month = 12-months, year = now.year-1)
	else:
		initial_date = now.replace(day=1, month=1)

	if request.user.is_designated_person:
		folio = request.user.poliza
		graph = TicketConPoliza.objects.filter(created_at__gte=initial_date, poliza=folio).annotate(month=TruncMonth('created_at')).values('month').annotate(c=Count('id')).order_by('-month')
		status = TicketConPoliza.objects.filter(poliza=folio).values('status').annotate(c = Count('status')).order_by('status')
		poliza = "poliza={0}".format(folio)
	else:
		graph = TicketConPoliza.objects.filter(created_at__gte=initial_date).annotate(month=TruncMonth('created_at')).values('month').annotate(c=Count('id')).order_by('-month')
		status = TicketConPoliza.objects.all().values('status').annotate(c = Count('status')).order_by('status')



	
	if not request.user.is_designated_person:
		graph2 = Poliza.objects.annotate(
			ticket_count=Subquery(
				TicketConPoliza.objects.filter(
					created_at__gte=now.replace(month=now.month-1),
					poliza=OuterRef('folio')
				).values('poliza')
				.annotate(cnt=Count('id'))
				.values('cnt'),
				output_field=IntegerField()
			)
		).order_by('-ticket_count')
	else:
		graph2 = None

	data = {
		'graph': graph,
		'status': status,
		'graph2': graph2,
		'poliza': poliza
	}

	return render(request, "home.html", data)

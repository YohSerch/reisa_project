import json
from io import TextIOWrapper, BytesIO
from datetime import date, timedelta
import calendar
import xlsxwriter


def load_json(request, redirect_to):
    try:
        file_json = TextIOWrapper(request.FILES['json'].file, encoding="utf-8")
        return json.loads(file_json.read())
    except ValueError:
        return -1


def date_generator():
    now = date.today()
    _, num_days = calendar.monthrange(now.year, now.month)
    return {
        'now': [
            now,
            now + timedelta(days=1)
        ],
        'last_7': [
            now - timedelta(days=7),
            now
        ],
        'this_month': [
            now.replace(day=1),
            now.replace(day=num_days)
        ],
        'this_year': [
            now.replace(month=1, day=1),
            now.replace(month=12, day=31)
        ]
    }


def filter_list(query, parameters):
    for k, vals in parameters.lists():
        for v in vals:
            if k != 'poliza' or k != 'equipo':
                query = query.filter(**{k: v})
    return query


def WriteToExcel(poliza, equipos):
    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Inventario")

    title_header = workbook.add_format({
        'bg_color': '#3498db',
        'font_color': '#FFFFFF',
        'bold': True,
        'font_size': 36,
        'align': 'center',
        'valign': 'vcenter'
    })

    header = workbook.add_format({
        'bg_color': '#3498db',
        'font_color': '#FFFFFF',
        'bold': True,
        'font_size': 16,
        'align': 'center',
        'valign': 'vcenter'
    })

    content = workbook.add_format({
        'font_size': 14,
        'align': 'center',
        'valign': 'vcenter'
    })

    heads = ['Id', 'Marca', 'Service Tag', 'Sistema Operativo', 'Disco Duro',
             'Ram', 'Graficos', 'Procesador', 'Software', 'Licencias']

    worksheet.set_column('A:H', 15)
    worksheet.set_column('D:D', 30)
    worksheet.set_column('G:G', 30)
    worksheet.set_column('H:H', 30)
    worksheet.set_column('I:I', 100)
    worksheet.set_column('J:J', 100)

    worksheet.merge_range('A1:J1', poliza.nombre, title_header)
    for i, head in enumerate(heads):
        worksheet.write(1, i, head, header)

    for i, equipo in enumerate(equipos):
        row = 2 + i
        software = ""
        licencias = ""
        worksheet.write(row, 0, equipo.id, content)
        worksheet.write(row, 1, equipo.marca, content)
        worksheet.write(row, 2, equipo.service_tag, content)
        worksheet.write(row, 3, equipo.sistema_operativo, content)
        worksheet.write(row, 4, equipo.disco_duro, content)
        worksheet.write(row, 5, equipo.ram, content)
        worksheet.write(row, 6, equipo.graficos, content)
        worksheet.write(row, 7, equipo.procesador, content)
        for s in equipo.software_set.all():
            software = software + s.nombre + ","
        for l in equipo.licencia_set.all():
            licencias = licencias + "{0}({1}),".format(l.software, l.codigo)
        worksheet.write(row, 8, software, content)
        worksheet.write(row, 9, licencias, content)

    workbook.close()
    return output.getvalue()

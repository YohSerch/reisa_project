from django.db import models
from django.contrib.auth.models import User, AbstractUser

from polizas.models import Poliza

class User(AbstractUser):
	telefono = models.CharField(max_length=50)
	is_designated_person = models.BooleanField(default=False)
	poliza = models.ForeignKey(Poliza, on_delete=models.CASCADE, blank=True, null=True)
	
	def __str__(self):
		return self.username

	class Meta:
		permissions = (("list_users", "Can list all the users"),)

# # Create your models here.
# class Encargado(models.Model):
# 	usuario = models.OneToOneField(User, on_delete=models.CASCADE)
# 	empresa = models.ForeignKey(Poliza, on_delete=models.CASCADE)

# 	def __str__(self):
# 		return "{0} - {1}".format(self.usuario.username, self.empresa.nombre)

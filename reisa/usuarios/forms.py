from django.forms import ModelForm, TextInput, PasswordInput, CheckboxInput, SelectMultiple, Select
from django.contrib.auth.models import Group

from .models import User


class UserCreateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = Group.objects.exclude(name="Encargados")

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'telefono',
                  'password', 'is_active', 'groups', 'poliza',
                  'is_designated_person']
        
        widgets = {
            'username': TextInput(attrs= {'placeholder': 'Nombre de usuario', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': 'Nombre', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Apellidos', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'telefono': TextInput(attrs={'placeholder': 'Telefono', 'class': 'form-control'}),
            'password': PasswordInput(attrs={'placeholder': 'Contraseña', 'class': 'form-control'}),
            'is_active': CheckboxInput(attrs={'class': 'custom-control-input'}),
            'is_designated_person': CheckboxInput(attrs={'class': 'custom-control-input'}),
            'poliza': Select(attrs={'class': 'custom-select', 'disabled': 'true', 'required': 'true'}),
            'groups': SelectMultiple(attrs={'class': 'form-control', 'required': 'true'})
        }


class UserUpdateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = Group.objects.exclude(name="Encargados")

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'telefono',
                  'is_active', 'groups', 'is_designated_person', 'poliza']
        widgets = {
            'username': TextInput(attrs= {'placeholder': 'Nombre de usuario', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': 'Nombre', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Apellidos', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'telefono': TextInput(attrs={'placeholder': 'Telefono', 'class': 'form-control'}),
            'is_active': CheckboxInput(attrs={'class': 'custom-control-input'}),
            'is_designated_person': CheckboxInput(attrs={'class': 'custom-control-input'}),
            'poliza': Select(attrs={'class': 'custom-select', 'required': 'true'}),
            'groups': SelectMultiple(attrs={'class': 'form-control', 'required': 'true'})
        }

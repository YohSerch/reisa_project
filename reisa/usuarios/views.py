from django.shortcuts import render, get_object_or_404, redirect
from django.http.response import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import Group

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .models import User
from .forms import UserCreateForm, UserUpdateForm

# Create your views here.
@login_required
def home(request, template_name="base.html"):
    return render(request, template_name, {})


class UserCreateView(SuccessMessageMixin, LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = User
    template_name = "usuarios/user_create_form.html"
    form_class = UserCreateForm
    success_url = reverse_lazy('usuarios:list')
    success_message = "Usuario %(username)s ha sido creado con éxito"
    permission_required = 'usuarios.add_user'
    raise_exception = True

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_password(form.cleaned_data["password"])
        self.object.save()
        if self.object.is_designated_person:
            encargados_group = Group.objects.get(name="Encargados")
            self.object.groups.add(encargados_group)
        else:
            self.object.groups.set(form.cleaned_data['groups'].all())
        self.object.save()
        return redirect(self.get_success_url())


class UserUpdateView(SuccessMessageMixin, LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = User
    template_name = "usuarios/user_update_form.html"
    form_class = UserUpdateForm
    success_url = reverse_lazy('usuarios:list')
    success_message = "Usuario %(username)s ha sido actualizado con éxito"
    permission_required = 'usuarios.change_user'
    raise_exception = True

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.groups.clear()
        if form.cleaned_data["is_designated_person"]:
            encargados_group = Group.objects.get(name="Encargados")
            self.object.groups.add(encargados_group)
        else:
            self.object.groups.set(form.cleaned_data["groups"].all())
        self.object.save()
        return redirect(self.get_success_url())


class UsersListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = User
    context_object_name = "users"
    template_name = "usuarios/users_list.html"
    permission_required = 'usuarios.list_users'
    raise_exception = True
    paginate_by = 8

    def get_queryset(self):
        users = User.objects.all().prefetch_related('groups', 'poliza').order_by('username')
        for k, vals in self.request.GET.lists():
            for v in vals:
                if k == "page":
                    continue
                users = users.filter(**{k:v})
        return users

    def get_context_data(self, **kwargs):
        context = super(UsersListView, self).get_context_data(**kwargs)
        if self.request.GET.get('is_active__exact'):
            context['status'] = "{0}={1}".format('is_active__exact', self.request.GET.get('is_active__exact'))
        if self.request.GET.get('groups__name__exact'):
            context['grupo'] = "{0}={1}".format('groups__name__exact', self.request.GET.get('groups__name__exact'))
        return context
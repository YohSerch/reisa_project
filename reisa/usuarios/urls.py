from django.conf.urls import url
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete, LoginView, LogoutView

from .views import *


urlpatterns = [
    url(r'^password/reset/$',
    	password_reset,
    	{'post_reset_redirect' : 'usuarios:password_reset_done'},
    	name="password_reset"),
    url(r'^password/reset/done/$',
        password_reset_done,
        name="password_reset_done"),
    url(r'^password/reset/(?P<uidb36>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 
        password_reset_confirm,
        {'post_reset_redirect': 'usuarios:password_success'},
        name="pasword_token"),
    url(r'^password/done/$', 
        password_reset_complete,
        name="password_success"),
    url(r'^login/$',
        LoginView.as_view(redirect_authenticated_user=True),
        name="login"),

    url(r'^logout/$',
        LogoutView.as_view(),
        {'next_page': '/'},
        name="logout"),

    url(r'^$',
        UsersListView.as_view(),
        name="list"),
    # Usuarios
    url(r'^create/$',
        UserCreateView.as_view(),
        name="create"),
    url(r'^update/(?P<pk>[\d]+)/$',
        UserUpdateView.as_view(),
        name="update")
]
import re

from django.contrib import auth
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = "Get a list of all permissions available in the system"

    def add_arguments(self, parser):
        parser.add_argument('app', nargs='+', type=str)

    def handle(self, *args, **kwargs):
        permissions = set()

        tmp_superuser = get_user_model()(is_active=True, is_superuser=True)

        for backend in auth.get_backends():
            # self.stdout.write(backend)
            if hasattr(backend, "get_all_permissions"):
                permissions.update(backend.get_all_permissions(tmp_superuser))

        sorted_list_of_permissions = sorted(list(permissions))


        if kwargs["app"]:
            r = re.compile("({0}).[A-Za-z_]+".format(kwargs["app"][0]))
            permissions = list(filter(r.match, sorted_list_of_permissions))


        self.stdout.write('\n'.join(permissions))

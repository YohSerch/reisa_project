$(document).ready(function() {
	$("#id_poliza").on('change', load_equipos);
});


function load_equipos(e) {
	equipos = $("#id_equipo");
	polizas = document.getElementById("id_poliza");
	$.ajax({
		url: '/inventarios/equipos',
		data: {
			'poliza': e.target.value
		},
		dataType: 'json',
		success: function(data) {
			let status = data.poliza_status ? 'is-valid' : 'is-invalid'
			polizas.classList.remove("is-valid", "is-invalid");
			polizas.classList.add(status);
			equipos.prop('disabled', false);
			equipos.empty();
			equipos.append("<option value='' selected=''>---------</option>");
			data.equipos.forEach(function(equipo) {
				equipos.append('<option value="' + equipo[0] +'">'+ equipo[0] + '</option>')
			});
		}
	})
}
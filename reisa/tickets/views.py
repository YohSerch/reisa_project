from datetime import datetime

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_safe
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import GroupRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404

from inventario.models import Equipo
from polizas.models import Poliza
from usuarios.models import User
from reisa.utils import date_generator, filter_list

from .models import TicketConPoliza
from .forms import TicketPolizaCreateForm, TicketPolizaUpdateForm, TicketPolizaCloseForm, TicketPolizaCreatebyEncargadoForm
# Create your views here.

class TicketPolizaCreateView(SuccessMessageMixin, LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = TicketConPoliza
    form_class = TicketPolizaCreateForm
    template_name = "tickets/ticket_create_form.html"
    permission_required = 'tickets.add_ticketconpoliza'
    raise_exception = True
    success_url = reverse_lazy('tickets:list')
    success_message = "Ticket creado con éxito"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_designated_person:
            return redirect('tickets:create_by_encargado')
        return super(TicketPolizaCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        ticket = form.instance
        ticket.registrado_por = self.request.user
        return super(TicketPolizaCreateView, self).form_valid(form)


class TicketPolizaEditView(SuccessMessageMixin, LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = TicketConPoliza
    form_class = TicketPolizaUpdateForm
    template_name = "tickets/ticket_update_form.html"
    success_url = reverse_lazy('tickets:list')
    permission_required = 'tickets.change_ticketconpoliza'
    raise_exception = True
    success_message = "Ticket modificado con éxito"

    def get_context_data(self, **kwargs):
        context = super(TicketPolizaEditView, self).get_context_data(**kwargs)
        context['equipos'] = Equipo.objects.filter(id_poliza = self.object.poliza)
        return context


class TicketPolizaListView(LoginRequiredMixin, ListView):
    model = TicketConPoliza
    context_object_name = "tickets"
    template_name = "tickets/ticket_list.html"
    paginate_by = 8
    user_group = None
    user_groups = []

    def get_queryset(self):
        self.user_groups = list(self.request.user.groups.values_list('name', flat=True))
        if "Encargados" in self.user_groups:
            tickets = TicketConPoliza.objects.select_related('poliza', 'registrado_por', 'equipo').filter(poliza = self.request.user.poliza)

        elif "Soporte" in self.user_groups and not "Soporte Administrador" in self.user_groups:
            tickets = TicketConPoliza.objects.select_related('poliza', 'registrado_por', 'equipo', 'atendido_por').filter(atendido_por=self.request.user)
        else:
            tickets = TicketConPoliza.objects.select_related('poliza', 'registrado_por', 'equipo').all()

        if self.request.GET.get('poliza'):
            tickets = tickets.filter(poliza = self.request.GET.get('poliza'))

        return filter_list(tickets, self.request.GET).order_by('-created_at', 'status')

    def get_context_data(self, **kwargs):
        context = super(TicketPolizaListView, self).get_context_data(**kwargs)
        context['polizas'] = Poliza.objects.all()

        if "Encargados" in self.user_groups:
            context['equipos'] = Equipo.objects.filter(id_poliza=self.request.user.poliza)

        if self.request.GET.get('poliza'):
            context['equipos'] = Equipo.objects.filter(id_poliza = self.request.GET.get('poliza'))
            context['url_poliza'] = "poliza={0}&".format(self.request.GET.get('poliza'))

        if self.request.GET.get('equipo') != "" and self.request.GET.get('equipo') != None:
            context['url_equipo'] = "equipo={0}&".format(self.request.GET.get('equipo'))

        if 'created_at__gte' in self.request.GET and 'created_at__lt' in self.request.GET:
            context['url_gte'] = "created_at__gte={0}&".format(self.request.GET.get('created_at__gte'))
            context['url_lt'] = "created_at__lt={0}&".format(self.request.GET.get('created_at__lt'))
        if 'status__exact' in self.request.GET:
            context['url_status'] = "status__exact={0}&".format(self.request.GET.get('status__exact'))

        context['dates'] = date_generator()
        context['user_group'] = self.user_group
        return context


class TicketPolizaDetailView(LoginRequiredMixin, DetailView):
    model = TicketConPoliza
    context_object_name = "ticket"
    template_name = "tickets/ticket_detail.html"

@login_required
@require_safe
def autorizar_servicio(request, pk):
    ticket = get_object_or_404(TicketConPoliza, id=pk)
    if not ticket.is_complete():
        messages.error(request, "El ticket no ha sido atendido, no puedes autorizarlo aún.")
        return redirect(request.GET.get('next'))
    ticket.autorizacion_cliente = True
    if request.user.encargado.empresa.status == False:
        ticket.status = '4'
    else:
        ticket.status = '5'
    ticket.save()
    messages.success(request, "Ticket autorizado con éxito!")
    return redirect(request.GET.get('next'))

@login_required
def asignar_soporte(request, pk):
    if request.method == "POST":
        asignado = False
        antiguo_asignado = None
        usuario = get_object_or_404(User, username = request.POST.get('soporte'))
        ticket = get_object_or_404(TicketConPoliza, id = pk)
        if ticket.atendido_por:
            asignado = True
            antiguo_asignado = ticket.atendido_por
        fecha_asignada = request.POST.get('fecha_programada')
        ticket.atendido_por = usuario
        ticket.fecha_programada = datetime.strptime(fecha_asignada, "%d/%m/%Y").strftime('%Y-%m-%d')
        ticket.hora_aproximada = datetime.strptime(request.POST.get('hora_programada'), "%H:%M").time()
        ticket.status = '2'
        ticket.tipo_de_atencion = request.POST.get('tipo_atencion')
        ticket.save()
        enviar_email(request, ticket, asignado, antiguo_asignado)
        messages.success(request, 'Soporte asignado con éxito')
        return redirect(request.GET.get('next'))
    else:
        soportes = User.objects.filter(groups__name = 'Soporte')
        ticket = get_object_or_404(TicketConPoliza, id = pk)
        # Si el ticket no ha sido atendido
        if ticket.status == '1' or ticket.status == '2':
            return render(request, "tickets/ticket_assign_soporte.html", {'soportes': soportes, 'ticket': ticket})
        # Si el ticket ya fue atendido enviamos un mensaje de error
        messages.error(request, 'El tiket ya fue atendido, no se le puede asignar de nuevo un soporte.')
        return redirect(request.GET.get('next'))


def enviar_email(request, ticket, reasignado, antiguo_soporte):
    domain = request.build_absolute_uri('/')[:-1]
    template_html = render_to_string("emails/support_template.html", {'poliza': ticket.poliza, 'ticket': ticket, 'equipo': ticket.equipo, 'domain': domain})
    template_txt = render_to_string("emails/support_template.txt", {'poliza': ticket.poliza, 'ticket': ticket, 'equipo': ticket.equipo, 'domain': domain})
    if reasignado:
        template_cancel = render_to_string("emails/cancel_template.txt", {'ticket': ticket})
        send_mail('Soporte reasignado', template_cancel, 'soporte@reisa.mx', [antiguo_soporte.email])

    send_mail('Tienes un nuevo soporte', template_txt, 'soporte@reisa.mx', [ticket.atendido_por.email], html_message=template_html)

@login_required
def concluir_soporte(request, pk):
    ticket = get_object_or_404(TicketConPoliza, id = pk)
    if ticket.status != '3':
        messages.error(request, 'El ticket aún no se puede concluir en su status actual.')
        return redirect('tickets:list')
    if request.method == "POST":
        form = TicketPolizaCloseForm(request.POST, instance = ticket)
        if form.is_valid():
            form.instance.status = '3'
            form.instance.fecha_atencion = datetime.now()
            form.save()
            messages.success(request, 'Ticket concluido con éxito, gracias por tu excelente trabajo ^.^')
            return redirect(request.GET.get('next'))
        else:
            return render(request, "tickets/ticket_close_soporte.html", {'ticket': ticket, 'form': form})
    form = TicketPolizaCloseForm(instance = ticket)
    return render(request, "tickets/ticket_close_soporte.html", {'ticket': ticket, 'form': form})

@login_required
@permission_required('tickets.add_ticketconpoliza', raise_exception = True)
def crear_ticket_por_encargado(request):
    equipos = Equipo.objects.filter(id_poliza=request.user.poliza)
    if request.method == "POST":
        form = TicketPolizaCreatebyEncargadoForm(request.POST)
        form.instance.poliza = request.user.poliza
        if form.is_valid():
            form.instance.registrado_por = request.user
            form.save()
            messages.success(request, 'Ticket {form.instance.id} registrado con éxito')
            return redirect('tickets:list')
        else:
            return render(request, "tickets/ticket_create_encargado_form.html", {'form': form, 'equipos': equipos})
    form = TicketPolizaCreatebyEncargadoForm()
    return render(request, "tickets/ticket_create_encargado_form.html", {'form': form, 'equipos': equipos})


@login_required
def autorizar_cobro(request, pk):
    ticket = get_object_or_404(TicketConPoliza, id = pk)
    ticket.status = '5'
    ticket.save()
    messages.success(request, 'Status actualizado con éxito.')
    return redirect(request.GET.get('next'))


@login_required
def buscar_ticket(request):
    if request.GET.get('ticket'):
        ticket = get_object_or_404(TicketConPoliza, id = request.GET.get('ticket'))
        return redirect('tickets:detail', ticket.id)
    else:
        raise Http404
    # return reverse_lazy('tickets:detail', kwargs={'pk': ticket.id})
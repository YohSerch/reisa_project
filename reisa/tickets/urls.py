from django.conf.urls import url

from .views import *

urlpatterns = [
	url(r'^$',
		TicketPolizaListView.as_view(),
		name="list"),
	url(r'^create/$',
		TicketPolizaCreateView.as_view(),
		name="create_ticket"),
	url(r'^createby/$',
		crear_ticket_por_encargado,
		name="create_by_encargado"),
	url(r'^detail/(?P<pk>[\d]+)/$',
		TicketPolizaDetailView.as_view(),
		name="detail"),
	url(r'^update/(?P<pk>[\d]+)/$',
		TicketPolizaEditView.as_view(),
		name="update"),
	url(r'^autorizar/(?P<pk>[\d]+)/$',
		autorizar_servicio,
		name="autorizar"),
	url(r'^asignar_soporte/(?P<pk>[\d]+)/$',
		asignar_soporte,
		name="asignar"),
	url(r'^concluir_soporte/(?P<pk>[\d]+)/$',
		concluir_soporte,
		name="concluir"),
	url(r'^autorizar_ticket/(?P<pk>[\d]+)/$',
		autorizar_cobro,
		name="cobro"),
	url(r'^search/',
		buscar_ticket,
		name="search")
]
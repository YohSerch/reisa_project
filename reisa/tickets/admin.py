from django.contrib import admin

from .models import TicketConPoliza
# Register your models here.

class TicketPolizaAdmin(admin.ModelAdmin):
	list_filter = ('poliza', 'status', 'created_at')


admin.site.register(TicketConPoliza, TicketPolizaAdmin)
from django.forms import ModelForm, Select
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from froala_editor.widgets import FroalaEditor

from .models import TicketConPoliza

class TicketPolizaCreateForm(ModelForm):
	class Meta:
		model = TicketConPoliza
		fields = ['reporte', 'poliza', 'equipo']
		widgets = {
			'poliza': Select(attrs={'class': 'form-control'}),
			'reporte': FroalaEditor(options={
				'placeholderText': 'Detalles de la incidencia'},
				attrs={'class': 'form-control'})
		}

class TicketPolizaCreatebyEncargadoForm(ModelForm):
	class Meta:
		model = TicketConPoliza
		fields = ['reporte', 'equipo']
		widgets = {
			'reporte': FroalaEditor(options={
				'placeholderText': 'Detalles de la incidencia'},
				attrs={'class': 'form-control'})
		}


class TicketPolizaUpdateForm(ModelForm):
	class Meta:
		model = TicketConPoliza
		fields = ['reporte', 'equipo']
		widgets = {
			'reporte': FroalaEditor(options={
				'placeholderText': 'Detalles de la incidencia'},
				attrs={'class': 'form-control'})
		}

class TicketPolizaCloseForm(ModelForm):
	class Meta:
		model = TicketConPoliza
		fields = ['solucion', 'notas_adicionales']
		widgets = {
			'solucion': FroalaEditor(options={
				'placeholderText': 'Detalles de la solución'},
				attrs={'class': 'form-control'}),
			'notas_adicionales': FroalaEditor(options={
				'placeholderText': 'Notas Adicionales'},
				attrs={'class': 'form-control'})
		}
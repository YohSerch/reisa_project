from django.db import models

from usuarios.models import User
from polizas.models import Poliza
from inventario.models import Equipo
from froala_editor.fields import FroalaField

# Create your models here.
class Ticket(models.Model):
	status = (
		('1', 'Pendiente'),
		('2', 'Asignado'),
		('3', 'Atendido'),
		('4', 'Sin cobro'),
		('5', 'Completado')
	)
	tipo_atencion = (
		('1', 'Remota (TeamViewer)'),
		('2', 'Telefónica'),
		('3', 'Presencial')
	)
	reporte = FroalaField()
	registrado_por = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='%(class)s_ticket_created')
	created_at = models.DateField(auto_now_add=True)
	status = models.CharField(max_length=1, choices=status, default='1')
	fecha_atencion = models.DateField(null=True)
	tipo_de_atencion = models.CharField(max_length=1, choices=tipo_atencion, default='1')
	atendido_por = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='%(class)s_ticket_attended')
	solucion = FroalaField(null=True)
	notas_adicionales = FroalaField(blank=True)
	fecha_programada = models.DateField(null=True)
	hora_aproximada = models.TimeField(null=True)

	def is_complete(self):
		return (self.status == '3' or self.status == '4' or self.status == '5')

	def is_attended(self):
		return self.status == '3'


class TicketConPoliza(Ticket):
	poliza = models.ForeignKey(Poliza, on_delete=models.CASCADE)
	equipo = models.ForeignKey(Equipo, on_delete=models.SET_NULL, null=True)
	autorizacion_cliente = models.BooleanField(default=False)

	class Meta:
		permissions = (("add_soporte", "Can add a soporte user"),
						("list_by_equipo", "Can list tickets by equipo"))

	def __str__(self):
		return "#{0} - {1} - {2}".format(self.id, self.poliza.nombre, self.equipo.id)



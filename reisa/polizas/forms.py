from django.forms import ModelForm, TextInput, DateInput, Textarea, CheckboxInput
from django.utils.translation import ugettext_lazy as _

from .models import Poliza, Contacto


class PolizaCreateForm(ModelForm):
	class Meta:
		model = Poliza
		fields = ['nombre','direccion', 'equipos_en_poliza', 'fecha_alta', 'ultima_revision_poliza', 'ultimo_servicio', 'status']
		widgets = {
			'nombre': TextInput(attrs={'placeholder': 'Nombre', 'class': 'form-control', 'autofocus': ''}),
			'direccion': Textarea(attrs={'placeholder': 'Dirección', 'class': 'form-control'}),
			'equipos_en_poliza': TextInput(attrs={'placeholder': 'Equipos en poliza', 'class': 'form-control'}),
			'fecha_alta': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker form-control'}),
			'ultima_revision_poliza': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker form-control'}),
			'ultimo_servicio': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker form-control'}),
			'status': CheckboxInput(attrs={'class': 'custom-control-input'})
		}
		labels = {
			'fecha_alta': _('Fecha de alta'),
			'ultima_revision_poliza': _('Última revisión de la poliza'),
			'ultimo_servicio': _('Último servicio anual'),
		}

class PolizaUpdateForm(ModelForm):
	class Meta:
		model = Poliza
		exclude = ('folio', 'telefono')
		widgets = {
			'nombre': TextInput(attrs={'placeholder': 'Nombre', 'class': 'form-control', 'autofocus': ''}),
			'direccion': Textarea(attrs={'placeholder': 'Dirección', 'class': 'form-control'}),
			'equipos_en_poliza': TextInput(attrs={'placeholder': 'Equipos en poliza', 'class': 'form-control'}),
			'fecha_alta': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker', 'class': 'form-control'}),
			'ultima_revision_poliza': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker form-control'}),
			'ultimo_servicio': DateInput(attrs={'placeholder': 'dd/mm/aaaa', 'class': 'datepicker form-control'}),
			'status': CheckboxInput(attrs={'class': 'custom-control-input'})
		}
		labels = {
			'fecha_alta': _('Fecha de alta'),
			'ultima_revision_poliza': _('Última revisión de la poliza'),
			'ultimo_servicio': _('Último servicio anual'),
		}

class ContactoCreateForm(ModelForm):
	class Meta:
		model = Contacto
		fields = ['nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'email']
		widgets = {
			'nombre': TextInput(attrs={'placeholder': 'Nombre', 'class': 'form-control'}),
			'apellido_paterno': TextInput(attrs={'placeholder': 'Apellido Paterno', 'class': 'form-control'}),
			'apellido_materno': TextInput(attrs={'placeholder': 'Apellido Materno', 'class': 'form-control'}),
			'telefono': TextInput(attrs={'placeholder': 'Télefono', 'class': 'form-control'}),
			'email': DateInput(attrs={'placeholder': 'Email', 'class': 'form-control'})
		}
from django.contrib import admin
from .models import Poliza, Contacto

# Register your models here.
class PolizaAdmin(admin.ModelAdmin):
	exclude = ('folio',)
	pass

class ContactoAdmin(admin.ModelAdmin):
	pass

admin.site.register(Poliza, PolizaAdmin)
admin.site.register(Contacto, ContactoAdmin)
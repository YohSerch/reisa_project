from django.db import models

# Create your models here.
class Poliza(models.Model):
	folio = models.CharField(max_length=20, null=False, primary_key=True)
	nombre = models.CharField(max_length=100, null=False)
	direccion = models.TextField(null=False)
	telefono = models.CharField(max_length=14, null=False)
	equipos_en_poliza = models.IntegerField(null=False)
	fecha_alta = models.DateField(null=False)
	ultima_revision_poliza = models.DateField(null=False)
	ultimo_servicio = models.DateField(null=False)
	status = models.BooleanField(default=True)

	class Meta:
		permissions = (('list_polizas', 'Can list all the polizas'),
						('list_equips', 'Can view the equips by poliza'))

	def save(self, *args, **kwargs):
		if self.folio is None or self.folio == "":
			nuevo_folio = "POL{0}".format(Poliza.objects.count() + 1)
			self.folio = nuevo_folio
		super(Poliza, self).save(*args, **kwargs)
	

	def __str__(self):
		return self.nombre


class Contacto(models.Model):
	id = models.AutoField(primary_key=True)
	nombre = models.CharField(max_length=50, null=False)
	apellido_paterno = models.CharField(max_length=50, null=False)
	apellido_materno = models.CharField(max_length=50, blank=True)
	telefono = models.CharField(max_length=50, null=False)
	email = models.EmailField(null=False)
	folio_poliza = models.ForeignKey(Poliza, on_delete=models.CASCADE)

	def __str__(self):
		return "{0} {1} {2}".format(self.nombre, self.apellido_paterno, self.apellido_materno)

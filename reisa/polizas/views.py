import sys
from datetime import datetime

from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_safe
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import PermissionDenied
from django.db.models import Count, Q
from django.db.models.functions import TruncMonth
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .models import Poliza, Contacto
from .forms import *
from reisa.utils import load_json
from tickets.models import TicketConPoliza
from inventario.models import Equipo


@login_required
@require_safe
def estadisticas_individuales(request, pk):
    # Si el usuario es encargado checamos que tenga la poliza asignada
    # si no mandamos 403
    if request.user.is_designated_person:
        folio = request.user.poliza.folio
        if folio != pk:
            raise PermissionDenied
    # Si no mostramos la poliza
    poliza = get_object_or_404(Poliza, folio=pk)
    # Tickets por mes 
    now = datetime.now()
    initial_date = now.replace(day=1, year=now.year-1)
    graph = TicketConPoliza.objects.filter(created_at__gte=initial_date, poliza=poliza).annotate(month=TruncMonth('created_at')).values('month').annotate(count=Count('id')).order_by('-month')
    print(graph)
    # Equipos dados de alta por año
    graph2 = Equipo.objects.filter(created_at__gte =initial_date, id_poliza=poliza).values('status').annotate(count=Count('status')).order_by('-status')

    return render(request, "polizas/polizas_statistics.html", {'poliza': poliza, 'graph': graph, 'graph2': graph2})


class PolizaListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Poliza
    context_object_name = "polizas"
    paginate_by = 8
    permission_required = 'polizas.list_polizas'
    raise_exception = True

    def get_queryset(self):
        return Poliza.objects.order_by('folio')


class PolizaDetailView(LoginRequiredMixin, DetailView):
    model = Poliza
    context_object_name = "poliza"

    def get_queryset(self):
        if self.request.user.is_designated_person:
            folio = self.request.user.poliza.folio
            if folio != self.kwargs['pk']:
                raise PermissionDenied
        return Poliza.objects.filter(folio=self.kwargs['pk']).annotate(equip_count=Count('equipo'))


class PolizaCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Poliza
    template_name = "polizas/poliza_create_form.html"
    form_class = PolizaCreateForm
    success_url = reverse_lazy('polizas:list')
    permission_required = 'polizas.add_poliza'
    raise_exception = True
    success_message = "La póliza %(nombre)s ha sido creada con éxito"


class PolizaUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Poliza
    form_class = PolizaUpdateForm
    success_url = reverse_lazy('polizas:list')
    template_name = "polizas/poliza_update_form.html"
    permission_required = 'polizas.change_poliza'
    raise_exception = True
    success_message = "La poliza %(nombre)s ha sido modificada con éxito"


class ContactoCreateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Contacto
    template_name = "polizas/contact_create_form.html"
    form_class = ContactoCreateForm
    permission_required = 'polizas.add_contacto'
    raise_exception = True
    success_message = "El contacto %(nombre)s ha sido creado con éxito"

    def form_valid(self, form):
        poliza = Poliza.objects.get(folio = self.kwargs['poliza'])
        poliza_id = poliza.folio
        form.instance.folio_poliza = poliza
        return super(ContactoCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('polizas:contacts', kwargs= {'poliza': self.kwargs['poliza']})


class ContactoUpdateView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Contacto
    template_name = "polizas/contact_create_form.html"
    form_class = ContactoCreateForm
    permission_required = 'polizas.change_contacto'
    raise_exception = True
    success_message = "El contacto %(nombre)s ha sido modificado con éxito"

    def get_success_url(self):
        return reverse_lazy('polizas:contacts', kwargs= {'poliza': self.kwargs['poliza']})


class ContactoDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Contacto
    template_name = "polizas/contact_delete_confirm.html"
    permission_required = 'polizas.delete_contacto'
    raise_exception = True
    success_message = "El contacto ha sido eliminado con éxito"

    def get_success_url(self):
        return reverse_lazy('polizas:contacts', kwargs= {'poliza': self.kwargs['poliza']})

class ContactoListView(LoginRequiredMixin, ListView):
    paginate_by = 8
    template_name = "polizas/contact_list.html"
    context_object_name = "contact_list"
    model = Contacto
    poliza = None
    
    def get_queryset(self):
        self.poliza = get_object_or_404(Poliza, folio = self.kwargs['poliza'])
        return Contacto.objects.filter(folio_poliza = self.poliza)

    def get_context_data(self, **kwargs):
        context = super(ContactoListView, self).get_context_data(**kwargs)
        context['poliza'] = self.poliza
        return context


@login_required
@permission_required('polizas.add_poliza', raise_exception=True)
def carga_polizas(request):
    if request.method == "POST":
        records = load_json(request, 'polizas:create_multiple')
        if records == -1:
            messages.error(request, "Formato de archivo invalido")
            return redirect('polizas:create_multiple')
        index = Poliza.objects.count()
        try:
             Poliza.objects.bulk_create([ Poliza(folio="POL{0}".format(index + i), nombre=line['nombre'].strip(), direccion=line['direccion'].strip(), telefono=line['telefono'].strip(), equipos_en_poliza=line['equipos'], fecha_alta=line['fecha_alta'].strip(), ultima_revision_poliza=line['ultima_revision_poliza'].strip(), ultimo_servicio=line['ultimo_servicio'].strip(), status=line['status']) for i, line in enumerate(records, start=1) ])
        except:
            messages.error(request, 'Ocurrio un error al almacenar las pólizas, revisa tu archivo e intenta de nuevo. {0}'.format(sys.exc_info()[1]))
            return redirect('polizas:create_multiple')
        
        return redirect('polizas:list')
        
    return render(request, "polizas/carga_masiva_form.html")




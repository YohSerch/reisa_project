from django.conf.urls import url
from .views import *

urlpatterns = [
    url(regex=r"^$", view=PolizaListView.as_view(), name="list"),
    url(r'^create/$', view=PolizaCreateView.as_view(), name="create"),
    url(r'^update/(?P<pk>[\w]+)/$', view=PolizaUpdateView.as_view(), name="update"),
    url(r'^(?P<pk>[\w]+)/$', PolizaDetailView.as_view(), name="detail"),
    url(r'^(?P<pk>[\w]+)/estadisticas/$', estadisticas_individuales, name="statistics"),
    url(r'^(?P<poliza>[\w]+)/contactos/create/$', view=ContactoCreateView.as_view(), name="contact_crate"),
    url(r'^(?P<poliza>[\w]+)/contactos/update/(?P<pk>[\d]+)/$', view=ContactoUpdateView.as_view(), name="contacto_update"),
    url(r'^(?P<poliza>[\w]+)/contactos/delete/(?P<pk>[\d]+)/$', view=ContactoDeleteView.as_view(), name="contacto_delete"),
    url(r'^(?P<poliza>[\w]+)/contactos', view=ContactoListView.as_view(), name="contacts"),
    url(r'^create/multiple/$', carga_polizas, name="create_multiple")
]